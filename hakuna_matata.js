
function main() {

    const inputs = [
        { person: 'A', start: '9:30', end: '10:30', cost: 1000 },
        { person: 'B', start: '9:30', end: '11:00', cost: 2000 },
        { person: 'C', start: '10:15', end: '11:00', cost: 1500 },
        { person: 'D', start: '12:00', end: '14:00', cost: 2500 },
        { person: 'E', start: '15:00', end: '16:00', cost: 1000 },
        { person: 'F', start: '12:30', end: '15:00', cost: 4000 },
        { person: 'G', start: '12:00', end: '15:00', cost: 5000 }
    ];

    const formattedInputs = formatTime(inputs);
    const sortedInputs = sortingAsc(formattedInputs);
    const reducedDublicates = reduceDublicates(sortedInputs);
    const sortedInputsPossibilities = possibilities(sortedInputs);
    const reducedDublicatesPossibilities = possibilities(reducedDublicates);
    const totalPossibilities = [...sortedInputsPossibilities, ...reducedDublicatesPossibilities];
    findScheduleForMoreIncome(totalPossibilities);

}

function formatTime(inputs) {
    let formattedValue = [];
    inputs.forEach((p) => {
        const input = {
            person: p.person,
            start: h_to_m(p.start),
            end: h_to_m(p.end),
            cost: p.cost
        }
        formattedValue.push(input);
    });

    return formattedValue;
}

function h_to_m(time) {
    const splitTime = time.split(":");
    return parseInt(splitTime[0]) * 60 + parseInt(splitTime[1]);
}

function sortingAsc(data) {
    data.sort(function (a, b) {
        return a.start - b.start;
    });

    return data;
}

function reduceDublicates(data) {
    const filteredValue = data.reduce((acc, current) => {
        const x = acc.find(item => item.start === current.start);
        if (!x) {
          return acc.concat([current]);
        } else {
            const y = acc.find(item => item.start === current.start && item.cost < current.cost);
            if (y) {
                const index = acc.findIndex(item => item.start === current.start && item.cost < current.cost);
                acc[index] = current;
                return acc;
            }
          return acc;
        }
      }, []);

      return filteredValue;
}

function possibilities(datas) {
    let pairs = [];

    for (let i = 0; i < datas.length; i++) {
        let com = '';
        let lastPushedVal = [];
        com = datas[i].person;
        lastPushedVal.push(datas[i]);
        cost = datas[i].cost;
        let combinatioLoop = i + 1;
        while(combinatioLoop < datas.length) {
            for (let j = combinatioLoop; j < datas.length; j++) {
                if (lastPushedVal[lastPushedVal.length - 1].end <= datas[j].start) {
                    com = `${com} + ${datas[j].person}`;
                    lastPushedVal.push(datas[j]);
                    cost = cost + datas[j].cost;
                }
    
            }
            combinatioLoop = combinatioLoop + 1;
        }
        pairs.push({person: com, cost: cost});
    }

    return pairs;

}

function findScheduleForMoreIncome(schedules) {
    schedules.sort((a, b) => {
        return a.cost - b.cost;
    });
    console.log('########## OUTPUT ##########');
    console.log(`Schedule For More Income :: ${schedules[schedules.length - 1].person} = ${schedules[schedules.length - 1].cost}`);
    console.log('########## THANK YOU ##########')
}

main();